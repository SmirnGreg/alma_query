from astroquery.alma import Alma
import numpy as np
import pickle
import astropy.table
import astropy.units as u
from astropy.coordinates import SkyCoord
from collections import OrderedDict

# object_list = [
#     'TW Hya', 'HD 163296', 'V4046 Sgr', 'FP Tau', 'UZ Tau', 'BP Tau', 'IM Lup', 'MWC 480', 'AS 209',
# ]

object_list = [
    'TW Hya', 'HD 163296', 'V4046 Sgr', 'AS 209',  'IM Lup', 'LkCa 15', 
]
# with open('temp','r') as file:
#     object_list = []
#     for line in file.readlines():
#         object_list.append(line.strip())
# object_list = ['GSS_31']
object_set = set(object_list)
object_list = list(object_set)
desired_frequencies = OrderedDict([
    ('HCO+_1-0', 89.18861227526095),
    ('HCO+_2-1', 178.37522884859138),
    ('HCO+_3-2', 267.5578918180973),
    ('HCO+_4-3', 356.73458708183057),
    ('HCO+_5-4', 445.9033383378792),
    ('HCO+_7-6', 624.2090628192366),
    ('HCO+_8-7', 713.3420640407029),
    ('HCO+_9-8', 802.4591762467995),
    ('HCO+_10-9', 891.5584144356056),
    ('H13CO+_1-0', 86.75437192053519),
    ('H13CO+_2-1', 173.50686813925597),
    ('H13CO+_3-2', 260.25559375432925),
    ('H13CO+_4-3', 346.99868266395004),
    ('H13CO+_5-4', 433.7342475662925),
    ('H13CO+_7-6', 607.1752883418714),
    ('H13CO+_8-7', 693.8770022114685),
    ('H13CO+_10-9', 867.2334129051818),
    ('DCO+_J=2-1', 144.0774243711267),
    ('DCO+_J=3-2', 216.11278905345188),
    ('DCO+_J=4-3', 288.1441367318913),
    ('DCO+_J=5-4', 360.1701284051497),
    ('DCO+_J=6-5', 432.18942507193174),
    ('DCO+_J=9-8', 648.1937630204752),
    ('DCO+_J=11-10', 792.1386482633922),
    ('DCO+_J=12-11', 864.0896778641369),
    ('DCO+_J=13-12', 936.0246494493484),
])
report_table = None
report_short = None
for obj in object_list:
    obs = None
    try:
        print('Loading obs...')
        with open('{obj}.pickle'.format(obj=obj), 'rb') as file:
            obs, list_loaded = pickle.load(file)
            if obs is None:
                continue
        # if set(list_loaded) != set(object_list):
        #     print('Object list have changed!')
        #     raise FileNotFoundError
        print('Loaded all entries for ', obj)
    except FileNotFoundError:
        print('Quering obs...')
        try:
            _obs = Alma.query_object(obj)
        except ValueError:
            print('{} not found!'.format(obj))
            with open('{obj}.pickle'.format(obj=obj), 'wb') as file:
                obs = None
                pickle.dump((obs, object_list), file)
            continue
        print(_obs)
        if obs is None:
            obs = _obs
        else:
            obs = astropy.table.vstack([obs, _obs])
        with open('{obj}.pickle'.format(obj=obj), 'wb') as file:
            pickle.dump((obs, object_list), file)
        print('Downloaded all entries for ', obj)

    if len(obs) == 0:
        continue
    good_data = obs  # [
    # ((obs['Band'] == 6) | (obs['Band'] == 7)) &
    # (obs['Integration'] > 600)
    # ]

    for eline in desired_frequencies.keys():
        good_data[eline] = 100 * ' '
        good_data[eline] = ''
    good_data['All frequencies'] = False
    good_data['Any frequency'] = 0
    not_interesting = []
    for row, entry in enumerate(good_data['Frequency support']):
        for subentry in entry.split('U'):
            parameters = subentry.strip('[] ').split(',')
            freq_0_str, freq_1_str = parameters[0].strip('GHz').split('..')
            freq_0, freq_1 = map(float, (freq_0_str, freq_1_str))
            for eline in desired_frequencies.keys():
                if freq_0 < desired_frequencies[eline] < freq_1:
                    # good_data[eline][row] += 1
                    print()
                    print()
                    print("Frequency {freq} was found in: \n{subentry}".format(
                        freq=desired_frequencies[eline], subentry=good_data[row])
                    )
                    print('in chunk', subentry)
                    good_data['Any frequency'][row] += 1

                    # Resolution parse

                    resolution, resolution_unit = float(parameters[1][:-3]), parameters[1][-3:]
                    res_dict_to_MHz = {
                        'GHz': 1.e3,
                        'MHz': 1.,
                        'kHz': 1.e-3,
                    }
                    try:
                        resolution *= res_dict_to_MHz[resolution_unit]
                    except KeyError:
                        print('Resolution unit {} not found!'.format(resolution_unit))
                    # if resolution > 1e-3: good_data['']
                    velocity_resolution = resolution * 0.001 / desired_frequencies[eline] * 3e5 # to kms
                    print('With velocity resolution ', velocity_resolution)
                    # Sensityvity parse
                    # '832.2uJy/beam@10km/s'
                    value_unit = parameters[2].split('/')[0]
                    sensitivity, sensitivity_unit = float(value_unit[:-3]), value_unit[-3:]
                    res_dict_to_uJy = {
                        'uJy': 1.,
                        'mJy': 1.e3,
                    }
                    try:
                        sensitivity *= res_dict_to_uJy[sensitivity_unit]
                    except KeyError:
                        print('Sensitivity unit {} not found!'.format(sensitivity_unit))
                    print('With sensitivity resolution ', sensitivity)
                    good_data[eline][row] += '{:5.2f}kms@{:5.2f}uJy'.format(velocity_resolution, sensitivity)
        if good_data['Any frequency'][row] == 0:\
            not_interesting.append(row)
    print('Removing not interesting rows: ', not_interesting)
    good_data.remove_rows(not_interesting)
    if len(good_data) == 0:
        print('No line found for ', obj)
        continue
    eline_number = len(desired_frequencies)

    for row, entry in enumerate(good_data):
        good_elines = 0
        for eline in desired_frequencies.keys():
            if entry[eline] is True: good_elines += 1
        if good_elines == eline_number:
            good_data['All frequencies'][row] = True
    # for frequency in desired_frequencies.keys():
    #     good_data = good_data[
    #         good_data[frequency] == True
    #         ]

    good_data['SkyCoord'] = [SkyCoord(ra=entry['RA'] * u.degree, dec=entry['Dec'] * u.degree) for entry in good_data]
    good_data['RA_h'] = [entry['RA'] / 15 for entry in good_data]
    print(good_data['SkyCoord'][0].ra.to_string(unit=u.hourangle, sep=':'))
    print(good_data['SkyCoord'][0].dec.to_string(unit=u.degree, sep=':'))

    good_data['RA_hms'] = [entry.ra.to_string(unit=u.hourangle, sep=':') for entry in good_data['SkyCoord']]
    good_data['Dec_dms'] = [entry.dec.to_string(unit=u.degree, sep=':') for entry in good_data['SkyCoord']]

    if report_table is None:
        report_table = good_data
    else:
        report_table = astropy.table.vstack((report_table, good_data))

    report_short_row = good_data[:0].copy()
    report_short_row.add_row()
    for i, column in enumerate(good_data.colnames):
        if column == 'Source name':
            report_short_row[column][0] = obj
        elif column == 'RA_hms':
            report_short_row[column][0] = good_data[column][0]
        elif column == 'Dec_dms':
            report_short_row[column][0] = good_data[column][0]
        elif column == 'Any frequency':
            report_short_row[column][0] = np.sum(good_data[column])
        elif column == 'Band':
            report_short_row[column][0] = [band.data[0] for band in good_data[column]]
        else:
            try:
                report_short_row[column][0] = np.sum(good_data[column])
            except TypeError:
                report_short_row[column][0] = ', '.join([str(entry) for entry in good_data[column]])
    if report_short is None:
        report_short = report_short_row
    else:
        report_short = astropy.table.vstack((report_short, report_short_row))
    print(report_short_row)
    print(report_short)

columns_show = ['Project code', 'Source name', 'RA_hms', 'Dec_dms', 'Band', 'Any frequency']
columns_show.extend((desired_frequencies.keys()))
report_table[columns_show].show_in_browser(
    jsviewer=True, max_lines=-1)
report_short[columns_show].show_in_browser(
    jsviewer=True, max_lines=-1, )
