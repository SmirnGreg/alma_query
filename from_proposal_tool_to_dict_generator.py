from astropy.io import ascii

table_in = ascii.read('lines_from_alma_proposal_tool.dat', delimiter="\s")

string_out = ''

for entry in table_in:
    string_out = string_out + "('{name}', {freq}), \n".format(name=entry['Transition'], freq=entry['Sky_Freq']*1e-9)

print(string_out)